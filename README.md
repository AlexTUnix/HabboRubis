**Habbo Rubis, what ?**
==

**Pour:** Habbo Retro<br>
**Ecrit en:** Ruby on Rails<br>
**Utilisant:** Skeleton<br>
**Préprocesseurs:** Sass<br>
**Date de sortie:** comment pourrais-je savoir ?

**Comment contribuer au projet ?**
==

```
git clone https://gitlab.com/AlexTUnix/HabboRubis.git
cd HabboRubis
git add lefichier
git commit -m "description"
git push -u origin master
```

**Avancement du projet**
==
**16/08/2016:** customisations de Skeleton, écriture des feuilles de style.
